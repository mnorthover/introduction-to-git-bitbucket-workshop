# Introduction to Git & Bitbucket Workshop

This repository contains the files needed for the LNCD workshop introducing Git and Bitbucket.

## Contributors

The following people have contributed to this workshop.

* [Nick Jackson](mailto:nijackson@lincoln.ac.uk)
* [Harry Newton](mailto:hnewton@lincoln.ac.uk)
* [Michael Northover](mailto:11208113@lincoln.ac.uk)
